# Area 4 - Deno WS AsyncIterator test
(Ignore repo name I'm too lazy to change it.)

This is testing how the AsyncIterators on the std/ws lib works.

It should be very easy to test... Key word is should.

This is to test how deno treats resources. It runs two tests to see how the iterators page through the data coming in.

And, survey says...

Test A:
Socket enters ws_handle 1
1: A
1: Test
Socket enters ws_handle 2
1: B
Socket exits ws_handle 1
Socket exits ws_handle 2

Test B:
Socket enters ws_handle 1
1: A
1: Test
Socket exits ws_handle 1
Socket enters ws_handle 2
2: B
Socket exits ws_handle 2

Useful.