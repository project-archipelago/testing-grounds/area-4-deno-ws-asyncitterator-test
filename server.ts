// Copyright 2018-2020 the Deno authors. All rights reserved. MIT license.
import { serve } from "https://deno.land/std@0.77.0/http/server.ts";
import { delay } from "https://deno.land/std@0.76.0/async/mod.ts"
import {
  acceptWebSocket,
  isWebSocketCloseEvent,
  isWebSocketPingEvent,
  WebSocket as ServerSocket,
} from "https://deno.land/std@0.77.0/ws/mod.ts";

async function handleWsA1(sock: ServerSocket) {
    console.log("A1 socket connected!");
    for await (const ev of sock) {
        if (typeof ev === "string") {
            // text message.
            console.log("wsA1:Text", ev);
            if (ev === "test") {
                handleWsA2(sock);
            }
        }
    }
}


async function handleWsA2(sock: ServerSocket) {
    console.log("A2 Test Running");
    for await (const ev of sock) {
        if (typeof ev === "string") {
            // text message.
            console.log("wsA2:Text", ev);
            console.log("Test Ended");
            break;
        }
    }
}

async function mainA() {
    /** websocket echo server */
    console.log(`websocket server A is running on :${port}`);
    const s = serve(`:${port}`)
    for await (const req of s) {
        const { conn, r: bufReader, w: bufWriter, headers } = req;
        acceptWebSocket({
            conn,
            bufReader,
            bufWriter,
            headers,
        }).then(handleWsA1)
        break;
    }
    await delay(3000);
    s.close();
}


async function handleWsB1(sock: ServerSocket) {
    console.log("B1 socket connected!");
    for await (const ev of sock) {
        if (typeof ev === "string") {
            // text message.
            console.log("wsB1:Text", ev);
            if (ev === "test") {
                break;
            }
        }
    }
    handleWsB2(sock);
}


async function handleWsB2(sock: ServerSocket) {
    console.log("B2 Test Running");
    for await (const ev of sock) {
        if (typeof ev === "string") {
            // text message.
            console.log("wsB2:Text", ev);
        }
    }
}

async function mainB() {
    /** websocket echo server */
    console.log(`websocket server B is running on :${port}`);
    const s = serve(`:${port}`)
    for await (const req of s) {
        const { conn, r: bufReader, w: bufWriter, headers } = req;
        acceptWebSocket({
            conn,
            bufReader,
            bufWriter,
            headers,
        }).then(handleWsB1)
        break;
    }
    await delay(3000);
    s.close();
}


async function testA() {
    let sock = new WebSocket(`ws://127.0.0.1:${port}/`);
    await delay(500);
    let message = 0;
    sock.send("A");
    await delay(500);
    sock.send("test");
    await delay(500);
    sock.send("B");
    await delay(500);
    sock.close();
}

const port = Deno.args[0] || "8080";
mainA();
await testA();
await delay(1000);
mainB();
await testA();
await delay(1000);
Deno.exit()


